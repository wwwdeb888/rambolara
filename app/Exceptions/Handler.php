<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {

        $this->renderable(function (Throwable $e) {
            $error_code = (int)($e->getCode() ?: 400);
            if($error_code != 404){
                $error_code = 400;
            }
            $response = [
            'success'     => false,
            'status_code' => $error_code,
            'message'     => $e->getMessage()." at line no ".$e->getLine()." in file ".$e->getFile(),
            ];
            return response($response,$error_code);
        });
        $this->reportable(function (Throwable $e) {
            $error_code = (int)($e->getCode() ?: 400);
            if($error_code != 404){
                $error_code = 400;
            }
            $response = [
            'success'     => false,
            'status_code' => $error_code,
            'message'     => $e->getMessage()." at line no ".$e->getLine()." in file ".$e->getFile(),
            ];
            return response($response,$error_code);
        });
    }
}
