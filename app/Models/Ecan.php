<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ecan extends Model
{
    use HasFactory;
	protected $table = 'ecan_masters';
}
