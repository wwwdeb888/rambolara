<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\UserAuthOtp;
use App\Models\User;
use App\Models\Ecan;
use Illuminate\Support\Facades\Http;

class AuthController extends ApiController
{

    public function sendOtpMessage(Request $request){
        if($request->has('type') && $request->type){
            $request->merge(['type' => strtoupper(trim($request->type))]);
        }
        $messages = ['auth_source.required' => 'Email or Mobile is required.'];
        if($request->type =='EMAIL'){
            $request->merge(['auth_source' => trim($request->email)]);
            $messages = ['auth_source.required' => 'Email is required.'];
        }
        if($request->type =='MOBILE'){
            $request->merge(['auth_source' => trim($request->mobile_number)]);
            $messages = ['auth_source.required' => 'Mobile No. is required.'];
        }
        $validator = Validator::make($request->all(), [
            'type'       => 'in:EMAIL,MOBILE',
            'auth_source' => 'required',
        ],$messages);

        try{
            if ($validator->fails()) {
              return $this->sendError($validator->errors()->first(),$validator->errors(), 500);
            }
            
            $check_otp_exists = UserAuthOtp::first();
            $random_id        = rand(10000,99999);
            $otp_code         = ("0".$random_id);
            $check_otp_exists = UserAuthOtp::where('auth_type',$request->type)
                                           ->where('auth_source',$request->auth_source)
                                           ->first();
            $add_otp                = $check_otp_exists ?? new UserAuthOtp();
            $add_otp->auth_type     = $request->type;
            $add_otp->auth_source   = $request->auth_source;
            $add_otp->auth_otp      = $otp_code;
            $add_otp->verified_at   = NULL;
            $add_otp->save();   

            try{
             // $this->sendOTP($otp_code,$mobile_number);
              $message = "OTP Sent Successfullly";
              $result['OTP']  = $otp_code;
              return $this->sendSuccess($result,$message);
            }catch(Exception $ex){
              return $this->sendError($ex->getMessage(),[],500);
            }

        }catch(Exception $ex){
              return $this->sendError($ex->getMessage(),[],500);
        }        
    }

    public function forgotPasswordOtpMessage(Request $request){        
        if($request->has('type') && $request->type){
            $request->merge(['type' => strtoupper(trim($request->type))]);
        }
        $messages = ['auth_source.required' => 'Email or Mobile is required.'];
        if($request->type =='EMAIL'){
            $request->merge(['auth_source' => trim($request->email)]);
            $messages = ['auth_source.required' => 'Email is required.'];
        }
        if($request->type =='MOBILE'){
            $request->merge(['auth_source' => trim($request->mobile_number)]);
            $messages = ['auth_source.required' => 'Mobile No. is required.'];
        }
        $validator = Validator::make($request->all(), [
            'type'       => 'in:EMAIL,MOBILE',
            'auth_source' => 'required',
        ],$messages);

        if ($validator->fails()) {
            return $this->sendError($validator->errors()->first(),$validator->errors(), 500);
        }

        $check_otp_exists = UserAuthOtp::where('auth_type',$request->type)
                                       ->where('auth_source',$request->auth_source)
                                       ->first();
        $random_id        = rand(10000,99999);
        $otp_code         = ("0".$random_id);

        $check_otp_exists = UserAuthOtp::where('auth_type',$request->type)
                                       ->where('auth_source',$request->auth_source)
                                       ->first();
        $add_otp                = $check_otp_exists ?? new UserAuthOtp();
        $add_otp->auth_type     = $request->type;
        $add_otp->auth_source   = $request->auth_source;
        $add_otp->auth_otp      = $otp_code;
        $add_otp->verified_at   = NULL;
        $add_otp->save(); 
        try{
          $message = "OTP Sent Successfullly";
          $result['OTP']  = $otp_code;
          return $this->sendSuccess($result,$message);
        }catch(Exception $ex){
          return $this->sendError($ex->getMessage(),[], 500);
        }
    }

    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => [ 
                            'required',
                            Rule::exists('users')->where(function ($query) {
                                    return $query->whereNotNull('email_verified_at');
                            }),
                        ],
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors()->first(),$validator->errors(), 422);
        }

        if (!auth()->attempt($validator->validated())) {
            return $this->sendError("Please use correct Email or Passsword",$validator->errors(), 401);
        }
     
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();
            $eanData= Ecan::where('loggeduserId',$user->id)->get();
            $success['name'] = $user->name;
            $result['access_token'] = $user->createToken('authToken')->accessToken;
            $message = "User Login Successfullly";
            $result['user']  = $user;
            if($eanData && count($eanData) > 0 ){
                $result['can']  = $eanData[0]->can;
                $result['isKycDone']  = $eanData[0]->isProvision===0?false:true;

            }else{
                $result['isKycDone']  = false;
                $result['can']=null;
            }
            return $this->sendSuccess($result,$message);
        }else{
            return $this->sendError("Please use correct Email or Passsword",[], 401);
        }       
    }

    public function loginOtpPin(Request $request){

        $validator = Validator::make($request->all(), [
            'pin' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors()->first(),$validator->errors(), 400);
        }

        if (!Auth::check()) {
            return $this->sendError("Please use correct Email or Passsword",$validator->errors(), 401);
        }
        $login_user = Auth::user();
        $user = User::where('id', $login_user->id)->first();
        if ($user) {
            if (Hash::check($request->pin, $user->pin)) {
                $eanData= Ecan::where('loggeduserId',$user->id)->get();
                $result['access_token'] = $user->createToken('authToken')->accessToken;
                $message = "User Login Successfullly";
                $result['user']  = $user;
                if($eanData && count($eanData) > 0 ){
                    $result['can']  = $eanData[0]->can;
                    $result['isKycDone']  = $eanData[0]->isProvision===0?false:true;

                }else{
                    $result['isKycDone']  = false;
                    $result['can']=null;
                }
               
            // api call to Nodes
                return $this->sendSuccess($result,$message);
            }else{
                return $this->sendError("Please use correct Pin",[], 401);
            }
        } else {
              return $this->sendError("User does not exist",[], 401);

        }       
    }  

    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required|min:6',
            'pan' => 'required',
            'dob' => 'required',
            'pin' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'device_id' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors()->first(),$validator->errors(), 400);
        }
        $current_time = date('Y-m-d H:i:s');        
        $check_user = User::where('email',$request->email)->first();

        if($check_user){
            $user                     = $check_user;
            $user->email              = $request->email;
            $user->first_name         = $request->first_name;
            $user->last_name          = $request->last_name;
            $user->pan                = $request->pan;
            $user->device_id          = $request->device_id;
            $user->mobile_number      = $request->mobile_number;
            $user->mobile_number_verified_at  = $current_time;
            $user->email_verified_at  = $current_time;
            $user->password  = Hash::make($request->password);
            $user->pin  = Hash::make($request->pin);
            $user->save();
        }else{
            $user = User::create([
                     'email'             => $request->email,
                     'first_name'        => $request->first_name,      
                     'last_name'         => $request->last_name,      
                     'pan'               => $request->pan,      
                     'device_id'         => $request->device_id,      
                     'mobile_number'     => $request->mobile_number,      
                     'email_verified_at' => $current_time,
                     'mobile_number_verified_at' => $current_time,
                     'password'          => Hash::make($request->password),
                     'pin'               => Hash::make($request->pin),
                    ]);
        }

        $user = User::where('device_id', $request->device_id)->first();
        if ($user) {
            if (Hash::check($request->pin, $user->pin)) {
                $result['access_token'] = $user->createToken('authToken')->accessToken;
                $message = "User Login Successfullly";
                $result['user']  = $user;
                return $this->sendSuccess($result,$message);
            }else{
                $result['access_token'] = $user->createToken('authToken')->accessToken;
                $message = "User Login Successfullly";
                $result['user']  = $user;
                return $this->sendSuccess($result,$message);
                //return $this->sendError("Please use correct Email or Passsword",[], 401);
            }
        } else {
              return $this->sendError("User does not exist",[], 401);

        }
       
        $message = 'User successfully registered';
        $result['user']  = $user;
        return $this->sendSuccess($result,$message);
    }


    public function verifyOTP(Request $request) {
        
        if($request->has('type') && $request->type){
            $request->merge(['type' => strtoupper(trim($request->type))]);
        }
        $messages = ['auth_source.required' => 'Email or Mobile is required.'];
        if($request->type =='EMAIL'){
            $request->merge(['auth_source' => strtoupper(trim($request->email))]);
            $messages = ['auth_source.required' => 'Email is required.'];
        }
        if($request->type =='MOBILE'){
            $request->merge(['auth_source' => strtoupper(trim($request->mobile_number))]);
            $messages = ['auth_source.required' => 'Mobile No. is required.'];
        }

         $validator = Validator::make($request->all(), [
            'type'       => 'in:EMAIL,MOBILE',
            'auth_source' => 'required',
            'otp'         => 'required',
           // 'is_login'    => "in"
        ],$messages);

    
        if ($validator->fails()) {
          return $this->sendError($validator->errors()->first(),$validator->errors(), 500);
        }
            
        $check_otp_exists = UserAuthOtp::where('auth_type',$request->type)
                                           ->where('auth_source',$request->auth_source)
                                           ->where('auth_otp',$request->otp)
                                           ->whereNull('verified_at')
                                           ->first();
        
        if(!$check_otp_exists){
            return $this->sendError('Please Enter Valid OTP',[], 400);
        }
        $current_time = date('Y-m-d H:i:s');
        if($check_otp_exists){
            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s'));
            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$check_otp_exists->updated_at);
            $diff_in_minutes = $to->diffInMinutes($from);
            if($diff_in_minutes > 5){
               return $this->sendError('Your OTP had expired. Please get new one.',[], 400);
            }else{
               $check_otp_exists->verified_at = $current_time;
               $check_otp_exists->save();
            }
        }
        //$check_user = User::where('email',$request->email)->first();
        $message = 'User OTP successfully verified';
        if($request->has('is_login') && $request->is_login){
           if($request->type =='EMAIL'){
            $request->merge(['auth_source' => strtoupper(trim($request->email))]);
                $user = User::where('email', $request->email)->first();
                if ($user) {
                   
                        $result['access_token'] = $user->createToken('authToken')->accessToken;
                        $message = "User Login Successfullly";
                        $result['user']  = $user;
                        return $this->sendSuccess($result,$message);
                } else {
                      return $this->sendError("User does not exist",[], 401);

                }
           }
           if($request->type =='MOBILE'){
                $user = User::where('mobile_number', $request->mobile_number)->first();
                if ($user) {
                   
                        $result['access_token'] = $user->createToken('authToken')->accessToken;
                        $message = "User Login Successfullly";
                        $result['user']  = $user;
                        return $this->sendSuccess($result,$message);
                } else {
                      return $this->sendError("User does not exist",[], 401);

                }
            }
        }else{
            $result  = ['data' => ""];
        }
        return $this->sendSuccess($result,$message);
    }

    public function logout() {        
        $user   = Auth::user();
        $user->tokens()->delete();
        $message = 'User successfully signed out';
        $result['user']  = [];
        return $this->sendSuccess($result,$message);
    }   
    
    public function userProfile() {
       
        $message = 'User profile fetched successfully';
        $user   = Auth::user();
        $user_id = $user->id;
        $user_details = null;
        if($user_id){
            $user_details = User::findOrFail($user_id);
        }
        $result  = $user_details;
        return $this->sendSuccess($result,$message);
    }

    public function updateUserProfile(Request $request) {
        
        $user    = Auth::user();
        $user_id = $user->id;
        $validator = Validator::make($request->all(), [
            'email'         => 'required|unique:users,email,'. $user_id,
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors()->first(),$validator->errors(), 400);
        }

        User::where('id',$user_id)->update([
             'email'         => $request->email,
             'name'          =>   $request->first_name,
             'first_name'        => $request->first_name,      
             'last_name'         => $request->last_name,      
             'pan'               => $request->pan,      
             'device_id'         => $request->device_id,      
             'mobile_number'     => $request->mobile_number,
        ]);


        $result['user']  = User::findOrFail($user_id);
        $message = "User saved successfully";
        return $this->sendSuccess($result,$message);
    }
    
    public function updateUserPassword(Request $request) {
       
        $user    = Auth::user();
        $message = 'User password updated successfully';
        $user_id = $user->id;
        $check_user = User::where('id',$user_id)
                          ->first();
        //  if (!Hash::check($request->current_password, $check_user->password)) {
        //     return $this->sendError("Please enter valid current password");
        // }else{
        User::where('id',$check_user->id)->update(['password' => Hash::make($request->password)]);
        $result['user']  = $check_user;
        return $this->sendSuccess($result,$message);
        //}
    }


    public function updateUserPin(Request $request) {    
        $user    = Auth::user();
        $message = 'User pin updated successfully';
        $user_id = $user->id;
        $check_user = User::where('id',$user_id)
                          ->first();
        
       User::where('id',$check_user->id)->update(['pin' => Hash::make($request->pin)]);
       $result['user']  = $check_user;
       return $this->sendSuccess($result,$message);        
    }

}