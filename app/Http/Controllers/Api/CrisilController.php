<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Traits\CrisilTrait;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Validator;

class CrisilController extends ApiController
{
    public function crisilData(Request $request){
      $validator = Validator::make($request->all(), [
        'date' => 'required',
      ]);
      if($validator->fails()){
        return $this->sendError($validator->errors()->first(),$validator->errors(), 400);
      }
    	$crisilAuth = CrisilTrait::getToken();
    	try{
        $result = [];
      	$message = "Crisil Data";
        if(isset($crisilAuth['access_token'])){
          $response = Http::withHeaders([
            'Content-Type: application/json',
            'Authorization' => "Bearer ".$crisilAuth['access_token']
          ])->post('https://beta.crisilquantix.com/api/feed/getRatingData', [
            'date' => $request->date,
          ]);
          if(isset($response['validationStatus']) && $response['validationStatus'] == "SUCCESS"){
      	    $result['crisilStatus']    = $response['validationStatus'];
            $result['data']            = $response['data'];
          }else{
            $result['crisilStatus']    = "FAILED";
            $result['data']            = "";
          }          
        }
      	return $this->sendSuccess($result,$message);
      }catch(Exception $ex){
       	return $this->sendError($ex->getMessage(),[],500);
      }
    }
}
