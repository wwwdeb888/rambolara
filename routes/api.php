<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CrisilController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    Route::post('login',             [AuthController::class, 'login']);
    Route::post('register',          [AuthController::class, 'register']);
    Route::post('send-otp',          [AuthController::class, 'sendOtpMessage']);
    Route::post('forgot/password',   [AuthController::class, 'forgotPasswordOtpMessage']);
    Route::post('verify/otp',        [AuthController::class, 'verifyOTP']);

    Route::middleware('auth:api')->group(function () {
        Route::post('login-pin',         [AuthController::class, 'loginOtpPin']);
        Route::get('logout',             [AuthController::class, 'logout']);
        Route::get('user/profile',       [AuthController::class, 'userProfile']);
        Route::post('user/profile',      [AuthController::class, 'updateUserProfile']);
        Route::post('update/password',   [AuthController::class, 'updateUserPassword']);
        Route::post('update/pin',        [AuthController::class, 'updateUserPin']);

        /* CRISIL Routes */
        Route::post('crisil',        [CrisilController::class, 'crisilData']);
        /* END */
    });